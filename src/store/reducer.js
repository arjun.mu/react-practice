const initialState = {
    counter: 0,
    userName: "John Doe"
};

const reducer = (state = initialState, action) => {
    if (action.type === 'RESET') {
        return {
            ...state, userName: action.value
        }
    }
    return state;
}

export default reducer;