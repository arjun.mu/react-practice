import React from 'react';

const contextData = React.createContext({
    key1: "Value 1",
    key2: "Value 2",
    key3: "Value 3"
});

export default contextData;