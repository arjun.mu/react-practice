import React, { Component } from 'react';
import './App.css';
import UserInput from './components/UserInput/UserInput';
import UserOutput from './components/UserOutput/UserOutput';
import ValidationComponent from './components/ValidationComponent';
import CharComponent from './components/CharComponent';
import ComponentUsingContext from './components/ComponentUsingContext';
import ContextData from './context/context-data';
import { connect } from 'react-redux';

class App extends Component {

  state = {
    userName: "FN LN",
    secondName: "Rob",
    count: 0
  };

  render(props) {
    return (
      <div className="App">
        <header onClick={this.updateName} className="App-header">
          <p>This is my first react app</p>
        </header>
        <UserInput
          userName={this.props.globalUserName}
          changeHandler={this.updateName} />
        <p>{this.state.count}</p>
        <UserOutput
          userName={this.props.globalUserName}
          click={this.updateName} />

        <input onChange={this.onChangeHandler}></input>
        <p>{this.state.secondName.length}</p>
        <ValidationComponent textLength={this.state.secondName.length} />
        {
          this.state.secondName.split('').map((x, i) => <CharComponent char={x} key={i} />)
        }
        <ContextData.Provider value={{ key1: 'new value 1', key2: 'new value 2' }}>
          <ComponentUsingContext />
        </ContextData.Provider>
      </div >
    );
  }

  updateName = (event) => {
    this.props.onInputChange(event.target.value);
  }

  onChangeHandler = (event) => {
    this.setState({
      secondName: event.target.value
    });
  }

}

const mapStateToProps = state => {
  return {
    globalUserName: state.userName
  };
}

const mapDispatchToProps = dispatch => {
  return {
    onInputChange: (value) => dispatch({ type: 'RESET', value })
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
