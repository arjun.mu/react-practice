import React from 'react';

class CharComponent extends React.Component {
    constructor() {
        super();
        this.charRef = React.createRef();
    }
    componentDidMount() {
        console.log(this.charRef.current);
    }

    render() {
        const style = {
            display: "inline-block",
            padding: "10px 18px",
            margin: "8px",
            border: "1px solid",
            cursor: "pointer"
        }
        return (
            <div style={style} >
                <p ref={this.charRef}>{this.props.char}</p>
            </div>
        );
    }

}

export default CharComponent;