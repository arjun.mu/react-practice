import React from 'react';
import PropTypes from "prop-types";
import './UserInput.css';

const UserInput = (props) => {
    return (
        <div className="user-input-wrapper">
            <input onChange={props.changeHandler} value={props.userName}></input>
        </div>
    );
}

UserInput.propTypes = {
    changeHandler: PropTypes.func,
    userName: PropTypes.string
};

export default UserInput;