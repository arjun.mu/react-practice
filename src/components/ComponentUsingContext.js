import React from 'react';
import ContextData from './../context/context-data';

class ComponentUsingContext extends React.Component {

    static contextType = ContextData;

    componentDidMount() {
        console.log(this.context);
    }

    render() {
        return Object.keys(this.context).map(k => <p key={Math.random()}>{`${k}: ${this.context[k]}`}</p>);
    }
};

export default ComponentUsingContext;